using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamageable
{
    [SerializeField] private List<Pickup> itemPool;
    [Range(0.0f, 1.0f)]
    [SerializeField] private float spawnRate;
    [SerializeField] private int itemsToSpawn;

    [SerializeField] protected float _Health;
    public float Health
    {
        get
        {
            return _Health;
        }
        set
        {
            if (value <= 0)
            {
                _Health = 0f;
                OnDeath();
            }
            else if (value > 100)
            {
                _Health = 100f;
            }
            else
            {
                _Health = value;
            }
        }
    }

    [SerializeField] protected bool _isAlive;
    public bool isAlive
    {
        get
        {
            return _isAlive;
        }
        set { _isAlive = value; }
    }

    private void GenerateItems()
    {
        float spawnChance = Random.value;

        if (spawnChance <= spawnRate)
        {
            for (int i = 0; i < itemsToSpawn; i++)
            {
                Instantiate(itemPool[Random.Range(0, itemPool.Count)], transform.position, Quaternion.identity);
            }
        }
    }

    public virtual void Movement()
    {

    }
    public virtual void Attack()
    {

    }

    public void GetDamaged(float damage)
    {
        Health -= damage;
    }

    public void OnDeath()
    {
        GenerateItems();
        Destroy(gameObject);
    }
}
