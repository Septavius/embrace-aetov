using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    public float Health { get; set; }

    public bool isAlive { get; set; }

    public void GetDamaged(float damage);

    public void OnDeath();
}
