using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_Health : Pickup
{
    [SerializeField] private float healthValue;
    // Update is called once per frame
    void Update()
    {
        updateLifetime();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Player_Inventory playerData = collision.GetComponentInChildren<Player_Inventory>();

            if(playerData != null)
            {
                playerData.InventoryAdd(this);
                Destroy(this.gameObject);
            }
        }
    }

    public override void ApplyEffect(Player_Controller playerData)
    {
        playerData.GetDamaged(-healthValue);
    }
}
