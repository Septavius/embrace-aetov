using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Player_Inventory : MonoBehaviour
{
    [SerializeField] public List<Pickups_Inventory> pickups = new List<Pickups_Inventory>();
    private int SelectionIndex = 0;
    private SpriteRenderer sr;
    [SerializeField] Sprite EmptyIcon;
    private float HideTime = 3f;
    private bool InventoryOpen;
    private Coroutine HideInventory;
    [SerializeField] private TextMeshPro amountText;
    private void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
        sr.enabled = false;
        amountText.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (pickups.Count > 0)
        {
            Inputs();
        }
    }

    public void Inputs()
    {
        if (pickups.Count > 0)
        {
            if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Q))
            {
                if (HideInventory != null)
                {
                    StopCoroutine(HideInventory);
                }
                HideInventory = StartCoroutine(HideNoInput());
                sr.enabled = true;
                amountText.gameObject.SetActive(true);
                InventoryOpen = true;

                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (SelectionIndex <= pickups.Count - 1)
                    {
                        SelectionIndex++;
                    }
                    if (SelectionIndex >= pickups.Count)
                    {
                        SelectionIndex = 0;
                    }
                    UpdateUI();
                    Debug.Log(SelectionIndex);
                }
                else if (Input.GetKeyDown(KeyCode.Q))
                {
                    if (SelectionIndex > 0)
                    {
                        SelectionIndex--;
                    }
                    else
                    {
                        SelectionIndex = pickups.Count - 1;
                    }
                    UpdateUI();
                    Debug.Log(SelectionIndex);
                }
            }
            if (Input.GetKeyDown(KeyCode.F) && InventoryOpen == true)
            {
                UsePickup(pickups[SelectionIndex]);
                UpdateUI();
            }
        }
    }
    private IEnumerator HideNoInput()
    {
        yield return new WaitForSeconds(HideTime);
        sr.enabled = false;
        amountText.gameObject.SetActive(false);
        InventoryOpen = false;
    }
    public void InventoryAdd(Pickup pickup)
    {
        Pickups_Inventory pickupToAdd = pickups.Find(p => p.pickup.GetType() == pickup.GetType());

        if (pickupToAdd == null)
        {
            pickups.Add(new Pickups_Inventory(pickup));
        }
        else if (pickupToAdd != null)
        {
            pickupToAdd.amount += pickup.Amount;
        }
    }

    public void UsePickup(Pickups_Inventory pickup)
    {
        pickup.ApplyEffect(gameObject.GetComponentInParent<Player_Controller>());

        pickup.amount--;

        if (pickup.amount <= 0)
        {
            pickups.RemoveAt(SelectionIndex);
            if (SelectionIndex > pickups.Count)
            {
                SelectionIndex = pickups.Count - 1;
            }
        }
    }

    private void UpdateUI()
    {
        if (pickups.Count == 0)
        {
            sr.sprite = EmptyIcon;
            amountText.text = "None";
        }
        else
        {
            sr.sprite = pickups[SelectionIndex].icon;
            amountText.text = pickups[SelectionIndex].amount.ToString();
        }
    }
}
