using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    [SerializeField]protected bool isActive;

    public bool IsActive
    {
        get { return isActive; }
    }

    public abstract void Activate();
}
