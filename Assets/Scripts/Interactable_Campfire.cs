using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_Campfire : Interactable
{
    public float range;
    public LayerMask playerLayer;
    public float lifeTime;
    private float lifeTimeValue;
    private Animator anim;

    [SerializeField] AudioClip campfireSFX;
    public override void Activate()
    {
        isActive = !isActive;
        anim.SetBool("isActive", isActive);

        if(isActive == true)
        {
            SFXManager.instance.playClipRange(campfireSFX, transform, range + 3);
        }
        else
        {
            SFXManager.instance.StopAudio(campfireSFX);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        isActive = false;
        lifeTimeValue = lifeTime;
    }

    private void Update()
    {
        if (isActive)
        {
            Collider2D[] col = Physics2D.OverlapCircleAll(transform.position, range, playerLayer);

            foreach(Collider2D c in col)
            {
                if (c.CompareTag("Player"))
                {
                    Player_Controller playerData = c.GetComponentInParent<Player_Controller>();

                    if(playerData != null)
                    {
                        playerData.CloseToHeatSource = true;
                        playerData.updateTempModifier();
                    }
                }
            }

            if(col.Length == 0)
            {
                OutOfRange();
            }

            DecreaseLifeTime();
        }
    }

    private void OutOfRange()
    {
        Player_Controller player = FindObjectOfType<Player_Controller>();

        if(player != null)
        {
            player.CloseToHeatSource = false;
            player.updateTempModifier();
        }
    }

    private void DecreaseLifeTime()
    {
        lifeTime -= Time.deltaTime;

        if(lifeTime <= 0)
        {
            Activate();
            lifeTime = lifeTimeValue;
        }
    }
}
