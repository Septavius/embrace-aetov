using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Projectile : Projectile
{
    private Weapon_Ranged playerWeapon;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerWeapon = (Weapon_Ranged)Player_Controller.currentWeapon;
        SetDirection(playerWeapon.AtkDirection.normalized);
    }
    private void Update()
    {
        if (Vector2.Distance(transform.position, playerWeapon.firePoint.position) > playerWeapon.Range)
        {
            Destroy(this.gameObject);
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            IDamageable enemyData = collision.gameObject.GetComponentInParent<IDamageable>();
            if (enemyData != null)
            {
                enemyData.GetDamaged(playerWeapon.Damage);
                Destroy(this.gameObject);
            }
        }
        base.OnTriggerEnter2D(collision);
    }
}
