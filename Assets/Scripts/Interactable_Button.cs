using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Interactable_Button : Interactable
{
    public TextMeshPro buttonText;
    private float HideTime = 1.5f;

    private void Start()
    {
        buttonText.gameObject.SetActive(false);
    }
    public override void Activate()
    {
        isActive = !isActive;
        buttonText.gameObject.SetActive(true);

        if (isActive)
        {
            buttonText.text = "Button: ON";
        }
        else
        {
            buttonText.text = "Button: OFF";
        }

        StartCoroutine(AutoHide());
    }

    private IEnumerator AutoHide()
    {
        yield return new WaitForSeconds(HideTime);
        buttonText.gameObject.SetActive(false);
    }
}
