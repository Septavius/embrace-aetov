using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Interactable_Button PowerButton;
    [SerializeField] public float speed;
    [SerializeField] bool GoBack = false;
    Rigidbody2D rb;
    private Vector2 direction;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        SetDirection();
    }
    // Update is called once per frame

    private void Update()
    {
        if (PowerButton.IsActive)
        {
            SetDirection();
        }
    }

    private void FixedUpdate()
    {
        if (PowerButton.IsActive)
        {
            rb.velocity = speed * direction;
        }
    }

    private void SetDirection()
    {
        if (GoBack)
        {
            direction = Vector2.left;
        }
        else
        {
            direction = Vector2.right;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("HOLA");

        if (collision.gameObject.CompareTag("Ground"))
        {
            GoBack = !GoBack;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.GetComponent<Player_Controller>().rb.velocity = rb.velocity;
        }
    }
}
