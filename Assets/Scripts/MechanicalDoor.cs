using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechanicalDoor : MonoBehaviour
{
    private Animator anim;
    public Button button;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("buttonActive", button.IsActive);
    }
}
