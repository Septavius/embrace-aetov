using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    [SerializeField] protected float lifetime;

    public int Amount { get; set; } = 1;
    protected void updateLifetime()
    {
        lifetime -= Time.deltaTime;
        
        if(lifetime <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public abstract void ApplyEffect(Player_Controller playerData);
}
