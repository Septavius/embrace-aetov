using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Interact : MonoBehaviour
{
    public LayerMask interactableLayer;
    private GameObject interactableInRange;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Collider2D[] col = Physics2D.OverlapCircleAll(transform.position, 1f, interactableLayer);

            foreach (Collider2D c in col)
            {
                if (c.gameObject.CompareTag("Interactable"))
                {
                    interactableInRange = c.gameObject;
                    Debug.Log(interactableInRange);

                    if(interactableInRange != null)
                    {
                        Interactable interactableData = interactableInRange.GetComponent<Interactable>();
                        interactableData.Activate();
                    }
                }
            }
        }
    }
}
