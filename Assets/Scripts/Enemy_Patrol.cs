using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Patrol : Enemy
{
    public List<Transform> patrolPoints;
    public int patrolPoints_Index;

    public float speed;
    public LayerMask playerLayer;

    public bool playerSpotted;
    public float spottingDistance;

    public Transform target;
    private Rigidbody2D rb;
    private Animator anim;
    private SpriteRenderer sr;
    // Start is called before the first frame update
    public void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    public void Update()
    {
        DetectPlayer();

        if (rb.velocity.x > 0)
        {
            anim.SetBool("isWalking", true);
            sr.flipX = false;
        }
        else
        {
            anim.SetBool("isWalking", true);
            sr.flipX = true;
        }
    }

    private void DetectPlayer()
    {
        Collider2D[] col = Physics2D.OverlapCircleAll(transform.position, spottingDistance, playerLayer);

        foreach (Collider2D c in col)
        {
            if (c.CompareTag("Player"))
            {
                target = c.gameObject.transform;
                playerSpotted = true;
                return;
            }
        }
        playerSpotted = false;
        target = null;

    }
    private void FixedUpdate()
    {
        if (playerSpotted == true)
        {
            Attack();
        }
        else if (playerSpotted == false)
        {
            Movement();
        }
    }
    public override void Movement()
    {
        Vector2 direction = (patrolPoints[patrolPoints_Index].position - transform.position).normalized;
        rb.velocity = new Vector2(direction.x * speed, rb.velocity.y);

        if (Vector2.Distance(transform.position, patrolPoints[patrolPoints_Index].position) < .4f)
        {
            patrolPoints_Index++;

            if (patrolPoints_Index > patrolPoints.Count - 1)
            {
                patrolPoints_Index = 0;
            }
        }
    }

    public override void Attack()
    {
        Vector2 direction = (target.position - transform.position).normalized;
        rb.velocity = new Vector2(direction.x * speed, rb.velocity.y);
    }
}
