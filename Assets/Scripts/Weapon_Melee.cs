using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Melee : Weapon
{
    private Animator anim;

    private RaycastHit2D[] hits;
    [SerializeField] Transform attackTransform;
    [SerializeField] private LayerMask damageableLayer;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public override void Attack()
    {
        Debug.Log("Attack!");
        anim.SetTrigger("isAttacking");
        hits = Physics2D.CircleCastAll(attackTransform.position, range, transform.right, 0f, damageableLayer);

        foreach (RaycastHit2D hit in hits)
        {
            IDamageable enemyData = hit.collider.gameObject.GetComponentInParent<IDamageable>();

            if (enemyData != null)
            {
                enemyData.GetDamaged(damage);
            }
        }
        attackCooldown = Time.time + 1f / AttackSpeed;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(attackTransform.position, range);
    }
}
