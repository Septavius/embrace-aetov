using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pickups_Inventory
{
    public Sprite icon;
    public int amount;
    public Pickup pickup;

    public Pickups_Inventory(Pickup pickup)
    {
        this.icon = pickup.GetComponent<SpriteRenderer>().sprite;
        this.amount = pickup.Amount;
        this.pickup = pickup;
    }

    public void ApplyEffect(Player_Controller target)
    {
        pickup.ApplyEffect(target);
    }
}
