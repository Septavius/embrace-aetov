using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_Ammo : Pickup
{
    [SerializeField] private int ammoValue;
    void Update()
    {
        updateLifetime();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player_Controller playerData = collision.GetComponent<Player_Controller>();
            Weapon wp = Player_Controller.currentWeapon;

            if(playerData != null && wp is Weapon_Ranged)
            {
                ApplyEffect(playerData);
                Destroy(gameObject);
            }
        }
    }

    public override void ApplyEffect(Player_Controller playerData)
    {
        Weapon_Ranged weaponData = (Weapon_Ranged)Player_Controller.currentWeapon;

        weaponData.gainAmmo(ammoValue);

        Destroy(this.gameObject);
    }
}
