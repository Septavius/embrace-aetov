using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_TextManager : MonoBehaviour
{
    public GameObject ammoText;
    private TextMeshProUGUI ammoInfo;
    private Weapon weapon;

    private void Start()
    {
        ammoInfo = ammoText.GetComponent<TextMeshProUGUI>();
    }
    void Update()
    {
        if(Player_Controller.currentWeapon != null)
        {
            weapon = Player_Controller.currentWeapon;

            if (weapon is Weapon_Ranged)
            {
                Weapon_Ranged weaponData = (Weapon_Ranged)weapon;
                ammoInfo.text = $"{weaponData.CurrentAmmo} / {weaponData.RemainingAmmo}";
            }
            else
            {
                ammoInfo.text = "";
            }
        }
    }
}
