using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBar_Health : UIBar
{
    Player_Controller playerData;
    void Start()
    {
        dataHolder = GameManager.player;
        playerData = dataHolder.GetComponent<Player_Controller>();
        UI_Fill = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        updateBar(playerData.Health);
    }
}
