using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PasscodeDoor : MonoBehaviour
{
    public Trigger codeTrigger;

    private GameObject codePanel;

    public TextMeshProUGUI codeText;

    public string PlayerCode;

    public string code;

    public string Code
    {
        get { return code; }
    }

    private Animator anim;

    private bool isOpen;

    public bool IsOpen
    {
        get { return isOpen; }
    }
    void Start()
    {
        codePanel = codeTrigger.itemToTrigger;
        anim = GetComponent<Animator>();
    }

    public void AddDigit(string digit)
    {
        if(PlayerCode.Length < 4)
        {
            PlayerCode += digit;
            UpdateCodeDisplay();
        }
    }

    public void ConfirmCode()
    {
        if(PlayerCode == code)
        {
            codePanel.SetActive(false);
            Destroy(codeTrigger);
            anim.SetBool("buttonActive", true);
        }
        else
        {
            ResetCode();
        }
    }

    private void UpdateCodeDisplay()
    {
        codeText.text = PlayerCode;
    }

    private void ResetCode()
    {
        PlayerCode = "";
        UpdateCodeDisplay();
    }
}
