using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{
    public static SFXManager instance;

    [SerializeField] private AudioSource SFXManagerObject; // PREFAB GLOBAL
    [SerializeField] private AudioSource SFXManagerRanged; //PREFAB CON CALCULADOR DE RANGO

    private List<AudioSource> activeAudioSources = new List<AudioSource>();

    private void Awake()
    {
       if(instance == null)
        {
            instance = this;
        }
    }

    public void playClip(AudioClip clip, Transform clipPosition, float volume) //CLIPS INDIFERENTES DE RANGO
    {
        AudioSource audioSource = Instantiate(SFXManagerObject, clipPosition.position, Quaternion.identity);

        audioSource.clip = clip;

        audioSource.volume = volume;

        activeAudioSources.Add(audioSource);

        audioSource.Play();

        float clipLength = audioSource.clip.length;

        StartCoroutine(DestroyAfterPlaying(audioSource, clipLength));
    }

    public void playClipRange(AudioClip clip, Transform clipPosition, float range) //CLIPS CON RANGO
    {
        AudioSource audioSource = Instantiate(SFXManagerRanged, clipPosition.position, Quaternion.identity);

        audioSource.clip = clip;

        audioSource.gameObject.GetComponent<Volume2D>().maxDistance = range; //Mando la distancia al calculador de volumen

        activeAudioSources.Add(audioSource);

        audioSource.Play();

        float clipLength = audioSource.clip.length;

        StartCoroutine(DestroyAfterPlaying(audioSource, clipLength));
    }

    public void playMusic(AudioClip clip, Transform clipPosition, float volume)
    {
        AudioSource audioSource = Instantiate(SFXManagerObject, clipPosition.position, Quaternion.identity);

        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.loop = true;

        activeAudioSources.Add(audioSource);

        audioSource.Play();
    }

    public void StopAudio(AudioClip clip)
    {
        for (int i = activeAudioSources.Count - 1; i >= 0; i--)
        {
            if (activeAudioSources[i] != null && activeAudioSources[i].clip == clip) //Busca el clip en la lista de clips activos
            {
                activeAudioSources[i].Stop();
                Destroy(activeAudioSources[i].gameObject);
                activeAudioSources.RemoveAt(i);
            }
        }
    }

    public void StopAllAudio()
    {
        foreach (var audioSource in activeAudioSources)
        {
            if (audioSource != null)
            {
                audioSource.Stop();
                Destroy(audioSource.gameObject);
            }
        }

        activeAudioSources.Clear();
    }

    private IEnumerator DestroyAfterPlaying(AudioSource audioSource, float audioLength)
    {
        yield return new WaitForSeconds(audioLength); //Espera a que termine el clip y destruye el objeto que gestiona el clip

        if (audioSource != null)
        {
            activeAudioSources.Remove(audioSource);
            Destroy(audioSource.gameObject);
        }
    }
}
