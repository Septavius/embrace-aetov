using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Volume2D : MonoBehaviour
{
    [SerializeField] GameObject listener;
    [SerializeField] public float maxDistance;
    [SerializeField] private float minDistance = 1f;
    private void Start()
    {
        listener = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        float listenDistance = Vector3.Distance(transform.position, listener.transform.position);

        if(listenDistance > maxDistance)
        {
            gameObject.GetComponent<AudioSource>().volume = 0f;
        }
        else if (listenDistance <= minDistance)
        {
            gameObject.GetComponent<AudioSource>().volume = 1f;
        }
        else
        {
            gameObject.GetComponent<AudioSource>().volume = 1 - ((listenDistance - minDistance) / (maxDistance - minDistance));
        }
    }
}
