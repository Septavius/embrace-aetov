using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static Scene currentScene;
    public static GameObject player;
    protected Player_Controller playerData;
    [SerializeField] public static bool gameOver;
    [SerializeField] public static bool WinLevel;

    public List<Transform> ZoneTriggers;
    public int ZoneTriggerIndex = 0;

    [SerializeField] public GameObject gameOverMenu;
    [SerializeField] public GameObject menuScreen;
    public List<DissapearingPlatforms> DPlatforms;

    [SerializeField] private AudioClip levelSong;
    [SerializeField] private AudioClip levelSFX;
    public GameObject playerPrefab;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        InitializeGame();
    }

    void InitializeGame()
    {
        currentScene = SceneManager.GetActiveScene();
        Debug.Log(currentScene.name + currentScene.buildIndex);

        InstantiatePlayer();  // Instanciar el nuevo prefab del jugador

        DPlatforms = new List<DissapearingPlatforms>();

        foreach (var platform in FindObjectsOfType<DissapearingPlatforms>())
        {
            DPlatforms.Add(platform);
        }

        AssignMenus();
        ResetGameState();
    }

    void InstantiatePlayer()
    {
        if (player != null)
        {
            Destroy(player);
        }
        player = Instantiate(playerPrefab);
        if (ZoneTriggers.Count > 0)
        {
            player.transform.position = ZoneTriggers[0].position;
        }
        playerData = player.GetComponent<Player_Controller>();
    }

    void AssignMenus()
    {
        if (gameOverMenu == null)
        {
            gameOverMenu = GameObject.Find("GameOverScreen");
        }

        if (menuScreen == null)
        {
            menuScreen = GameObject.Find("MenuScreen");
        }
    }

    void ResetGameState()
    {
        Time.timeScale = 1f;
        WinLevel = false;
        gameOver = false;

        if (gameOverMenu != null)
        {
            gameOverMenu.SetActive(false);
        }

        if (menuScreen != null)
        {
            menuScreen.SetActive(false);
        }
    }

    void Start()
    {
        SFXManager.instance.playMusic(levelSong, transform, 0.6f);
        SFXManager.instance.playMusic(levelSFX, transform, 0.6f);
        Debug.Log(playerData);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        CheckStatus();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (menuScreen != null)
            {
                menuScreen.SetActive(!menuScreen.activeInHierarchy);
            }
        }
    }

    private void CheckStatus()
    {
        if (playerData != null && !playerData.isAlive)
        {
            gameOver = true;
            if (gameOverMenu != null)
            {
                gameOverMenu.SetActive(true);
            }
            Time.timeScale = 0f;
        }

        if (WinLevel == true)
        {
            SFXManager.instance.StopAllAudio();
            Time.timeScale = 0f;
        }

        foreach (DissapearingPlatforms platform in DPlatforms)
        {
            if (platform.gameObject.activeInHierarchy == false)
            {
                platform.UpdateReappearingTime(Time.deltaTime);
            }
        }
    }

    public void ClearZone(ZoneTrigger zt)
    {
        zt.ActivateTrigger();
        ZoneTriggerIndex++;
    }

    public void Reload()
    {
        gameOver = false;
        Time.timeScale = 1f;

        playerData.isAlive = true;
        playerData.GetDamaged(-100);
        playerData.GainTemperature(100);
        player.transform.position = ZoneTriggers[ZoneTriggerIndex - 1].position;

        foreach (DissapearingPlatforms platform in DPlatforms)
        {
            platform.ReloadState();
        }

        if (gameOverMenu != null)
        {
            gameOverMenu.SetActive(false);
        }
    }

    public virtual void Restart()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(currentScene.buildIndex + 1);
    }
}