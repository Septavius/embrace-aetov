using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Attack : MonoBehaviour
{
    public float attackDamage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            IDamageable playerData = collision.GetComponentInParent<IDamageable>();
            Player_Controller playerKBData = collision.GetComponentInParent<Player_Controller>();

            if(playerData != null)
            {
                playerData.GetDamaged(attackDamage);
            }

            if(playerKBData != null)
            {
                playerKBData.KBTime = playerKBData.KBTotalTime;
                playerKBData.KBDirection = (collision.gameObject.transform.position - transform.position).normalized;
            }
        }
    }
}
