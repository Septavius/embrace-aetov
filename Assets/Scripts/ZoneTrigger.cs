using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneTrigger : MonoBehaviour
{
    [SerializeField] private bool clearZone;

    public bool ClearZone
    {
        get { return clearZone; }
    }

    public GameManager gm;

    private void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    public void ActivateTrigger()
    {
        clearZone = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (clearZone == false)
            {
                gm.ClearZone(this);
            }
        }
    }
}
