using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    [SerializeField] protected float speed;

    [SerializeField] protected Vector2 direction;

    protected Rigidbody2D rb;

    public void SetDirection(Vector2 newDirection)
    {
        direction = newDirection.normalized;

        if (rb != null)
        {
            rb.velocity = direction * speed;
        }
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            Destroy(this.gameObject);
        }
    }
}
