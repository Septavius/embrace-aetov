using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : GameManager
{
    [SerializeField] private GameObject TutorialLimit;

    public static bool TutorialPassed = false;

    protected override void Update()
    {
        if(playerData.Temperature == 100)
        {
            TutorialPassed = true;
        }

        if (TutorialPassed)
        {
            Destroy(TutorialLimit);
        }

        base.Update();
    }

    public override void Restart()
    {
        TutorialPassed = false;
        base.Restart();
    }
}
