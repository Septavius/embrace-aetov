using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Turret : Enemy
{
    [SerializeField] private bool HorizontalShot;
    [SerializeField] private bool LeftToRight; //En caso vertical, UpToDown

    public Interactable_Button PowerButton;
    public Transform firePoint;
    public GameObject projectile;
    private GameObject currentProjectile;

    // Update is called once per frame
    void Update()
    {
        if(PowerButton.IsActive == false)
        {
            Attack();
        }
    }

    public override void Attack()
    {
        if(currentProjectile == null)
        {
            currentProjectile = Instantiate(projectile, firePoint.position, Quaternion.identity);

            if (HorizontalShot)
            {
                if (LeftToRight)
                {
                    currentProjectile.GetComponent<Projectile_Enemy>().SetDirection(Vector2.right);
                }
                else
                {
                    currentProjectile.GetComponent<Projectile_Enemy>().SetDirection(Vector2.left);
                }
            }
            else
            {
                if (LeftToRight)
                {
                    currentProjectile.GetComponent<Projectile_Enemy>().SetDirection(Vector2.down);
                }
                else
                {
                    currentProjectile.GetComponent<Projectile_Enemy>().SetDirection(Vector2.up);
                }
            }

            currentProjectile.GetComponent<Projectile_Enemy>().setRange(999);
        }
    }
}
