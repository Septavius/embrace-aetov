using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Ranged : Weapon
{
    [SerializeField] public int CurrentAmmo;
    [SerializeField] public int MaxAmmo;
    [SerializeField] public int RemainingAmmo;

    [SerializeField] private AudioClip weaponSound;

    public GameObject projectile;
    [SerializeField] public Transform firePoint;

    private void Start()
    {
        CurrentAmmo = MaxAmmo;
    }
    public override void Attack()
    {
        if(CurrentAmmo > 0)
        {
            SFXManager.instance.playClip(weaponSound, firePoint.transform, 1f);

            Instantiate(projectile, firePoint.position, Quaternion.identity);
            CurrentAmmo--;
            attackCooldown = Time.time + 1f / AttackSpeed;
        }
    }

    public void Reload()
    {
        int missingAmmo = MaxAmmo - CurrentAmmo;

        if(RemainingAmmo > missingAmmo)
        {
            RemainingAmmo -= missingAmmo;
            CurrentAmmo = MaxAmmo;
        }
        else if (RemainingAmmo < missingAmmo)
        {
            CurrentAmmo += RemainingAmmo;
            RemainingAmmo = 0;
        }
    }

    public void gainAmmo(int ammoValue)
    {
        if(CurrentAmmo < MaxAmmo)
        {
            int missingAmmo = MaxAmmo - CurrentAmmo;
            if(ammoValue > missingAmmo)
            {
                CurrentAmmo = MaxAmmo;
                ammoValue -= missingAmmo;
            }
            else
            {
                CurrentAmmo += ammoValue;
                ammoValue = 0;
            }
        }
        RemainingAmmo += ammoValue;
    }
}
