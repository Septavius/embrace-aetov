using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour, IDamageable
{
    public Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator anim;
    #region Movement-Related
    [SerializeField] public float movespeed;
    [SerializeField] private float jumpPower;
    private float direction;
    private bool jumpQueue;
    public bool OnRope = false;
    public Vector2 boxSize;
    public float groundCastDistance;
    public LayerMask groundLayer;

    public Vector2 KBDirection { get; set; }
    public float KBForce;
    public float KBTime;
    public float KBTotalTime;
    #endregion
    #region Temperature-Related
    [SerializeField] private float temperature;

    public float Temperature
    {
        get { return temperature; }
    }

    [SerializeField] public float tempModifier;

    [SerializeField] public bool CloseToHeatSource { get; set; }

    #endregion
    #region Combat-Related

    private float _Health = 100f;
    public float Health
    {
        get { return _Health; }

        set
        {
            if (value <= 0)
            {
                _Health = 0f;
                OnDeath();
            }
            else if(value > 100)
            {
                _Health = 100f;
            }
            else
            {
                _Health = value;
            }
        }
    }
    private bool _isAlive = true;
    public bool isAlive
    {
        get
        {
            return _isAlive;
        }
        set
        {
            _isAlive = value;
        }
    }

    [SerializeField] private List<Weapon> weapons = new List<Weapon>();

    public static Weapon currentWeapon;

    [SerializeField] private Transform GunHoldTransform;

    private Vector3 gunPointDirection;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    #endregion
    void Start()
    {
        Debug.Log(Health);
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        if(GameManager.currentScene.buildIndex == 1)
        {
            temperature = 1f;
        }
        else
        {
            temperature = 100f;
        }
        if(GameManager.currentScene.buildIndex == 1)
        {
            tempModifier = 1f;
        }
        else if (GameManager.currentScene.buildIndex == 2)
        {
            tempModifier = 2.5f;
        }
        currentWeapon = weapons[0];

        UpdateWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.gameOver == false && GameManager.WinLevel == false)
        {
            InputRead();
            gunRotation();

            if (TutorialManager.TutorialPassed || GameManager.currentScene.buildIndex != 1)
            {
                UpdateTemperature(tempModifier);
            }
        }
    }

    private void FixedUpdate()
    {
        #region Movement-Related
        if (KBTime <= 0)
        {
            rb.velocity = new Vector2(direction * movespeed, rb.velocity.y);
            if (direction < 0 || direction > 0)
            {
                anim.SetBool("isWalking", true);
            }
            else
            {
                anim.SetBool("isWalking", false);
            }
        }
        else
        {
            rb.velocity = KBDirection * KBForce;
            KBTime -= Time.deltaTime;
        }
        if (jumpQueue)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            jumpQueue = false;
        }
        #endregion
    }

    private void InputRead()
    {
        #region Movement-Related
        direction = Input.GetAxisRaw("Horizontal");

        if(Input.GetKey(KeyCode.W) && OnRope)
        {
            rb.velocity = Vector2.zero;
            rb.AddForce(Vector2.up * movespeed, ForceMode2D.Impulse);
        }

        if (Input.GetKey(KeyCode.Space) && GroundCheck())
        {
            jumpQueue = true;
        }
        #endregion
        #region Combat-Related

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentWeapon = weapons[0];
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentWeapon = weapons[1];
        }

        UpdateWeapon();

        if (Input.GetMouseButton(0))
        {
            if (currentWeapon != null)
            {
                if (Time.time >= currentWeapon.AttackCooldown)
                {
                    if (currentWeapon is Weapon_Melee)
                    {
                        currentWeapon.Attack();
                    }
                    else if (currentWeapon is Weapon_Ranged)
                    {
                        currentWeapon.AtkDirection = gunPointDirection;
                        currentWeapon.Attack();
                    }
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (currentWeapon != null)
            {
                if (currentWeapon is Weapon_Ranged)
                {
                    ((Weapon_Ranged)currentWeapon).Reload();
                }
            }
        }
        #endregion
    }

    public bool GroundCheck()
    {
        if (Physics2D.BoxCast(transform.position, boxSize, 0, -transform.up, groundCastDistance, groundLayer))
        {
            anim.SetBool("isJumping", false);
            return true;
        }
        else
        {
            anim.SetBool("isJumping", true);
            return false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position - transform.up * groundCastDistance, boxSize);
    }
    #region Temperature-Related
    public void UpdateTemperature(float updateValue)
    {
        temperature -= updateValue * Time.deltaTime;
        if (temperature > 100)
        {
            temperature = 100f;
        }

        if (temperature <= 0)
        {
            temperature = 0;
            UpdateHealth(1);
        }
    }

    public void UpdateHealth(float updateValue)
    {
        Health -= updateValue * Time.deltaTime;
    }

    public void GainTemperature(float tempValue)
    {
        temperature += tempValue;

        if (temperature > 100)
        {
            temperature = 100f;
        }
    }
    public void updateTempModifier()
    {
        if (CloseToHeatSource == true)
        {
            tempModifier = -2;
        }
        else if (CloseToHeatSource == false)
        {
            tempModifier = 1;
        }
    }
    #endregion
    #region Combat-Related

    private void gunRotation()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        gunPointDirection = mousePosition - GunHoldTransform.position;
        gunPointDirection.z = 0f;

        float angle = Mathf.Atan2(gunPointDirection.y, gunPointDirection.x) * Mathf.Rad2Deg;

        GunHoldTransform.rotation = Quaternion.Euler(0, 0, angle);

        if (gunPointDirection.x < 0)
        {
            GunHoldTransform.localScale = new Vector3(1, -1, 1);
            sr.flipX = true;

        }
        else if (gunPointDirection.x > 0)
        {
            GunHoldTransform.localScale = new Vector3(1, 1, 1);
            sr.flipX = false;
        }
    }

    private void UpdateWeapon()
    {
        foreach (Weapon weapon in weapons)
        {
            weapon.gameObject.SetActive(weapon == currentWeapon);
        }
    }

    public void GetDamaged(float damage)
    {
        Health -= damage;
        Debug.Log(Health);
    }

    public void OnDeath()
    {
        isAlive = false;
    }
    #endregion
}
