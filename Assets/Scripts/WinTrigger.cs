using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTrigger : Trigger
{
    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManager.WinLevel = true;
        }
        base.OnTriggerEnter2D(other);
    }
}
