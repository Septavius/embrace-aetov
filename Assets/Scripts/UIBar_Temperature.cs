using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBar_Temperature : UIBar
{
    Player_Controller playerData;

    private void Start()
    {
        dataHolder = GameManager.player;

        if(playerData == null)
        {
            playerData = dataHolder.GetComponent<Player_Controller>();
        }

        UI_Fill = this.gameObject.GetComponent<Image>();
    }
    void Update()
    {
        updateBar(playerData.Temperature);
    }
}
