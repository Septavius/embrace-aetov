using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] protected float damage;

    public float Damage
    {
        get { return damage; }
    }

    [SerializeField] protected float range;

    public float Range
    {
        get { return range; }
    }

    protected Vector2 atkDirection;

    public Vector2 AtkDirection
    {
        get { return atkDirection; }
        set { atkDirection = value; }
    }

    [SerializeField] protected float attackSpeed;

    public float AttackSpeed
    {
        get { return attackSpeed; }
    }

    protected float attackCooldown;

    public float AttackCooldown
    {
        get { return attackCooldown; }
    }


    public abstract void Attack();

}
