using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingPad : MonoBehaviour
{
    public float bouncePower;
    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(collision.gameObject.transform.position.y > transform.position.y)
            {
                anim.SetTrigger("isUsed");
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector3.up * bouncePower, ForceMode2D.Impulse);
                StartCoroutine(ResetTrigger());
            }
        }
    }

    private IEnumerator ResetTrigger()
    {
        yield return new WaitForSeconds(0.1f);
        anim.ResetTrigger("isUsed");
    }
}
