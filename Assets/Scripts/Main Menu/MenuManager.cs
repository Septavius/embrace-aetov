using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject MainMenuScreen;
    public GameObject LevelSelectorScreen;
    void Start()
    {
        MainMenuScreen.SetActive(true);
        LevelSelectorScreen.SetActive(false);
    }

    public void TriggerMenu(GameObject menu)
    {
        MainMenuScreen.SetActive(!MainMenuScreen.activeInHierarchy);
        menu.SetActive(!menu.activeInHierarchy);
    }

    public void LevelSelect(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void ShutDown()
    {
        Application.Quit();
    }
}
