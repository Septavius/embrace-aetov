using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : Interactable
{
    private Animator anim;
    private float distanceToDeactivate = 1.5f;
    public override void Activate()
    {
        isActive = !isActive;
        anim.SetBool("isActivated", isActive);

    }
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        isActive = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.transform.position.y > transform.position.y)
        {
            if(!isActive)
            {
                Activate();
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.transform.position.y > transform.position.y)
        {
            if(Vector2.Distance(collision.gameObject.transform.position, transform.position) > distanceToDeactivate)
            {
                Activate();
            }
        }
    }
}
