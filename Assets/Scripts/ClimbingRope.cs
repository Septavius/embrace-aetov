using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingRope : Interactable
{
    Player_Controller player;
    public override void Activate()
    {
        if(player != null)
        {
            isActive = true;
            player.OnRope = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(player);

        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.gameObject.GetComponentInParent<Player_Controller>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isActive = false;
        player.OnRope = false;
        player = null;
    }
}
