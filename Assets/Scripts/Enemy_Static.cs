using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Static : Enemy
{
    public float detectionRange;
    private Transform target;

    public LayerMask detectionLayer;
    public LayerMask groundLayer;

    public GameObject projectile;
    private GameObject currentProjectile;

    public void Update()
    {
        DetectPlayer();
    }

    private void DetectPlayer()
    {
        Collider2D[] col = Physics2D.OverlapCircleAll(transform.position, detectionRange, detectionLayer);
        foreach (Collider2D c in col)
        {
            if (c.gameObject.CompareTag("Player"))
            {
                target = c.gameObject.transform;
                Attack();
                return;
            }
            target = null;
        }
    }

    public override void Attack()
    {
        if(target != null)
        {
            Vector2 fireDirection = target.position - transform.position;
            float distance = fireDirection.magnitude;
            fireDirection.Normalize();

            RaycastHit2D hit = Physics2D.Raycast(transform.position, fireDirection, distance, detectionLayer);
            RaycastHit2D GroundHit = Physics2D.Raycast(transform.position, fireDirection, distance, groundLayer);

            Debug.DrawRay(transform.position, fireDirection, Color.red);

            if(hit.collider != null && GroundHit.collider == null)
            {
                if(currentProjectile == null || currentProjectile.Equals(null))
                {
                    currentProjectile = Instantiate(projectile, transform.position, Quaternion.identity);
                    Projectile_Enemy projectileEnemy = currentProjectile.GetComponent<Projectile_Enemy>();

                    if(currentProjectile != null)
                    {
                        projectileEnemy.SetDirection(fireDirection);
                        projectileEnemy.setRange(detectionRange + 5f);
                    }
                }
            }
        }
    }
}
