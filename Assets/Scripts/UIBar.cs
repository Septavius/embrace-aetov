using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBar : MonoBehaviour
{
    public GameObject dataHolder;
    protected Image UI_Fill;

    public void updateBar(float dataValue)
    {
        UI_Fill.fillAmount = dataValue / 100f;
    }
}
