using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Enemy : Projectile
{
    [SerializeField] private float range;
    [SerializeField] Vector2 startPoint;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startPoint = transform.position;
        rb.velocity = direction * speed;
    }
    private void Update()
    {
        if (Vector2.Distance(startPoint, transform.position) > range)
        {
            Destroy(this.gameObject);
        }
    }

    public void setRange(float range)
    {
        this.range = range;
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
        base.OnTriggerEnter2D(collision);
    }
}
