using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;
    public Player_Controller playerData;

    public Vector3 targetPosition = Vector3.zero;
    public float cameraSpeed;
    public float offset;
    public float AheadDistance;
    public float AheadSpeed;

    public bool isFalling;
    public float MaxHeightOffset;
    private void Start()
    {
        player = GameManager.player;
        if(player != null)
        {
            playerData = GameManager.player.GetComponent<Player_Controller>();
            targetPosition = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
        }
    }
    void LateUpdate()
    {
        if (playerData.GroundCheck())
        {
            targetPosition.y = player.transform.position.y;
        }

        if(transform.position.y - player.transform.position.y > MaxHeightOffset || transform.position.y - player.transform.position.y < MaxHeightOffset)
        {
            isFalling = true;
        }

        if(isFalling)
        {
            targetPosition.y = player.transform.position.y;

            if (playerData.GroundCheck())
            {
                isFalling = false;
            }
        }

        if(playerData.rb.velocity.x > 0f)
        {
            offset = Mathf.Lerp(offset, AheadDistance, AheadSpeed * Time.deltaTime);
        }

        if(playerData.rb.velocity.x < 0f)
        {
            offset = Mathf.Lerp(offset, -AheadDistance, AheadSpeed * Time.deltaTime);
        }

        targetPosition.x = player.transform.position.x + offset;

        transform.position = Vector3.Lerp(transform.position, targetPosition, cameraSpeed * Time.deltaTime);
    }
}
