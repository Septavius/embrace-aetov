using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissapearingPlatforms : MonoBehaviour
{
    public float lifetime;
    private float OriginalLifetime;
    public float ReappearingTime;
    private float OriginalReappearingTime;

    [SerializeField] AudioClip iceSFX;

    bool playerLanded = false;

    private void Start()
    {
        OriginalLifetime = lifetime;
        OriginalReappearingTime = ReappearingTime;
    }

    private void Update()
    {
        if (playerLanded)
        {
            if (lifetime > 0)
            {
                lifetime -= Time.deltaTime;
            }
        }

        if (lifetime <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    public void ReloadState()
    {
        playerLanded = false;
        lifetime = OriginalLifetime;
        gameObject.SetActive(true);
        ReappearingTime = OriginalReappearingTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.transform.position.y > transform.position.y)
            {
                if (playerLanded == false)
                {
                    playerLanded = true;
                    SFXManager.instance.playClipRange(iceSFX, transform, 5);
                }
            }
        }
    }

    public void UpdateReappearingTime(float deltaTime)
    {
        if (!gameObject.activeInHierarchy)
        {
            ReappearingTime -= deltaTime;

            if (ReappearingTime <= 0)
            {
                ReloadState();
            }
        }
    }
}
